//
// Created by 12097 on 2023/10/29.
//

#ifndef PT_MOTO_H
#define PT_MOTO_H

#define  R1 HAL_GPIO_ReadPin(R1_GPIO_Port,R1_Pin)
#define  R2 HAL_GPIO_ReadPin(R2_GPIO_Port,R2_Pin)
#define  R3 HAL_GPIO_ReadPin(R3_GPIO_Port,R3_Pin)
#define  R4 HAL_GPIO_ReadPin(R4_GPIO_Port,R4_Pin)
#define  R5 HAL_GPIO_ReadPin(R5_GPIO_Port,R5_Pin)
#define  R6 HAL_GPIO_ReadPin(R6_GPIO_Port,R6_Pin)
#define  R7 HAL_GPIO_ReadPin(R7_GPIO_Port,R7_Pin)
#define  R7 HAL_GPIO_ReadPin(R7_GPIO_Port,R7_Pin)
#define  R8 HAL_GPIO_ReadPin(R8_GPIO_Port,R8_Pin)
#define  R9 HAL_GPIO_ReadPin(R9_GPIO_Port,R9_Pin)

#define  LED_UP HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,GPIO_PIN_SET);


#include "App.h"

class Moto {
public:
    Moto();
    uint8_t step{};
    float rref{},lref{};
    static float v;
    static float vbuf;
    static void init();
    void speed(float r,float l) const;
    void Moto_app();
private:
    static uint8_t stop_in_corner();
    static uint8_t stop_in_corner_front();
    static uint8_t stop_in_center();
    static uint8_t stop_in_round_center();
    static uint8_t stop_in_line();
    static uint8_t stop_outof_line();
    static uint8_t fine();
    static uint8_t stop();

    void debug() const;
    void inway(uint8_t out(void )) const;
    void inway_bcak(uint16_t t) const;
    void inway_frout(uint16_t t) const;
    void inway_left() const;
    void inway_right() const;
    void inway_crossing() const;
    void inway_crossing_center()const;
    void inway_stop_in_round_center();

    void bcak(uint16_t t) const;
    void frout(uint16_t t) const;

    void left(uint8_t out(void)) const;
    void right(uint8_t (*out)()) const;
    void left_black_from_home() const;
    void right_white_from_home() const;
    void left_white_from_home() const;
    void right_black_from_home() const;
};


#endif //PT_MOTO_H
