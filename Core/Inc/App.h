//
// Created by 12097 on 2023/10/29.
//

#ifndef PT_APP_H
#define PT_APP_H


#ifdef __cplusplus
extern "C" {
#endif


#include "main.h"
#include "cmsis_os.h"
#include"stdio.h"
#include "tim.h"
#include "usart.h"

extern float distance;

void StartDefaultTask(void const * argument);
void StartTask(void const * argument);
void SRTask(void const * argument);
void LEDTask(void const * argument);

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

#ifdef __cplusplus
}
#endif


#endif //PT_APP_H
