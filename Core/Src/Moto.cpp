//
// Created by 12097 on 2023/10/29.
//

#include "Moto.h"
#include "tim.h"

float  Moto::v;
float  Moto::vbuf;

Moto::Moto(){
    v=0;
}

void Moto::Moto_app() {
    /*
    switch (step) {
        case 0:
            break;
        case 1:
            inway(stop_in_corner);
            inway_frout((uint16_t)(500/v));
        case 2:
            inway(stop_in_corner);
            inway_crossing();
            inway_left();
            inway_left();
            left(stop_in_line);
            inway(stop_in_center);

            bcak(700);
            left(stop_in_line);
            inway_right();
            inway_right();
            right(stop_in_line);
            inway_crossing();
            inway(stop_in_corner);
            inway_frout((uint16_t)(100/v));
        case 3:
            left(stop_in_line);
            bcak(400);
            left(stop_in_line);
            inway_crossing();
            inway_right();
            inway_stop_in_round_center();
        case 4:
            left(stop_in_line);
            inway_left();
            left(stop_in_line);
            inway_crossing();
            inway_crossing_center();
            right(stop_in_line);
            inway_crossing();
            inway_right();
            inway_right();
            inway_stop_in_round_center();
        case 5:
            left(stop_in_line);
            inway_left();
            left(stop_in_line);
            inway_crossing();
            inway_crossing_center();
            right(stop_in_line);
            right(stop_in_line);
            inway_crossing();
            inway_stop_in_round_center();
        case 6:
            right(stop_in_line);
            inway(stop_in_corner);
            bcak(300);
            right(fine);
            left_black_from_home();
            inway_stop_in_round_center();
        case 7:
            left(stop_in_line);
            inway_crossing();
            inway_crossing_center();
            left(stop_in_line);
            right(fine);
            right_black_from_home();
            inway_stop_in_round_center();
    }//*/

    /*
    switch (step) {
        case 0:
            break;
        case 1:
            inway(stop_in_corner);
            inway_frout((uint16_t)(500/v));
            inway(stop_in_corner);
        case 2:
            left(stop_in_line);
            inway_crossing();
            inway_frout(2500);
            left(stop_in_line);
            inway_crossing();
            inway_crossing_center();
            left(stop_in_line);
            inway_crossing();
            inway_stop_in_round_center();
        case 3:
            left(stop_in_line);
            inway(stop_in_corner);
            inway_crossing();
            inway_right();
            inway_right();
            inway_stop_in_round_center();
        case 4:
            left(stop_in_line);
            inway_left();
            left(stop_in_line);
            inway_crossing();
            inway_crossing_center();
            left(stop_in_line);
            left(stop_in_line);
            inway_crossing();
            inway_frout(2500);
            left(stop_in_line);
            inway_crossing();
            inway_crossing_center();
            right(stop_in_line);
            inway_crossing();
            inway_stop_in_round_center();
        case 5:
            right(stop_in_line);
            inway(stop_in_corner);
            right(fine);
            left_white_from_home();
            inway_stop_in_round_center();
        case 6:
            right(stop_in_line);
            inway_crossing();
            inway_crossing_center();
            right(stop_in_line);
            right(fine);
            right_black_from_home();
            inway_stop_in_round_center();
    }//*/

    switch (step) {
        case 0:
            inway_crossing_center();
            left(fine);
            break;
        case 1:
            inway(stop_in_corner);
            inway_frout((uint16_t)(500/v));
            inway(stop_in_corner);
        case 2:
            left(stop_in_line);
    }//*/
    osDelay(1);
}

/*
 * 初始化
 */
void Moto::init(){
    HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4);
    HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_3);
}
/*
 * 修改速度
 */
void Moto::speed(float r,float l) const {
    r=r*rref*v;
    l=l*lref*v;
    TIM5->ARR=20000+1500+(int16_t)r;
    TIM5->CCR3=1500+(int16_t)r;
    TIM2->ARR=20000+1500-(int16_t)l;
    TIM2->CCR4=1500-(int16_t)l;
}
/*
 * 车轮在正中心的拐角处停下
 * 判断逻辑为r5r6r7扫到黑线,
 */
uint8_t Moto::stop_in_corner(){
    static uint8_t  i=0;
    if(R5==1&&R6==1&&R7==1)i++;
    else i=0;
    if(i>100){i=0;return 0;}
    return 1;
}
/*
 * 车头在正中心的拐角处停下
 * 判断逻辑为r1r2r3扫到黑线,
 */
uint8_t Moto::stop_in_corner_front(){
    static uint16_t  i=0;
    if(R1==1&&R2==1&&R3==1)i++;
    else i=0;
    if(i>300){i=0;return 0;}
    return 1;
}

/*
 * 车头在白色圆处停下
 * 判断逻辑为r2扫到白色超过150ms,
 */

uint8_t Moto::stop_in_center(){
    static uint8_t p=0;
    if(R2==0){
        p++;
        if(p>150){
            p=0;
            return 0;
        }
    }
    else p=0;
    return 1;
}

/*
 * 车头在靶环处停下
 * 判断逻辑为r2扫到白色超过10ms,
 */

uint8_t Moto::stop_in_round_center(){
    static uint8_t s=0;
        if(R2==0){
            s++;
            if(s>5){s=0;return 0;}
        }
        else s=0;
    return 1;
}

/*
 * 车头在扫到第一根线处停下
 * 判断逻辑为r2扫到黑色超过10ms,
 */

uint8_t Moto::stop_in_line(){
    static uint8_t  i=0;
    i=R2?i+1:0;
    if(i>10){i=0;return 0;}
    return 1;
}

/*
 * 车头在扫到第一根线，且车头离开该线时停下
 * 判断逻辑为r2扫到黑色超过10ms，且r2离开该线,
 */

uint8_t Moto::stop_outof_line(){
    static uint8_t  i=0,p=1;
    if(p){
        i=R2?i+1:0;
        if(i>10){p=0;i=0;}
    }else{
        i=R2?0:i+1;
        if(i>10){p=1;i=0;return 0;}
    }
    return 1;
}
/*
 * 超声波在扫物块停下
 * 判断逻辑为距离小于20cm大于200ms
 */

uint8_t Moto::fine(){
    static uint8_t i=0;
    i=distance<20?i+1:0;
    if(i>50){i=0;return 0;}
    return 1;
}
/*
 * 无用函数
 */
uint8_t Moto::stop(){
    static uint32_t  i=0;
    i=R2?0:i+1;
    if(i>1000)return 0;
    return 1;
}
/*
 * 循迹前进任务
 */
void Moto::inway(uint8_t out(void)) const {
    for(;out();){
        if(R1==0)speed(100,50);
        else if(R3==0)speed(50,100);
        else  speed(100,100);
        osDelay(1);
    }
}

/*
 * 循迹前进延时任务
 */
void Moto::inway_frout(uint16_t t) const {
    for (uint16_t i = 0; i < t; i++) {
        if (R1 == 0)speed(100, 50);
        else if (R3 == 0)speed(50, 100);
        else speed(100, 100);
        osDelay(1);
    }
}

/*
 * 循迹后退任务
 */
void Moto::inway_bcak(uint16_t t) const {
    for(uint16_t i=0;i<t;i++){
        if(R8==0)speed(50,100);
        else if(R9==0)speed(100,50);
        else  speed(100,100);
        osDelay(1);
    }
}
/*
 * 不循迹后退任务
 */
void Moto::bcak(uint16_t t) const {
    speed(-70,-70);
    osDelay(t);
    speed(0,0);
}
/*
 * 不循迹前进任务
 */
void Moto::frout(uint16_t t) const {
        speed(100, 120);
        osDelay(t);
}
/*
 * 左转任务
 */
void Moto::left(uint8_t out(void)) const {
    Moto::speed(-50,50);
    osDelay(300);
    for(;out();){
        osDelay(1);
    }
    speed(0,0);
}
/*
 * 右转任务
 */
void Moto::right(uint8_t (*out)()) const {
    Moto::speed(50,-50);
    osDelay(300);
    for(;out();){
        osDelay(1);
    }
    speed(0,0);
}
/*
 * 先循迹前进，在六边形顶点处右转任务
 */
void Moto::inway_right() const {
    inway(stop_in_center);
    frout((uint16_t)(700/v));
    Moto::speed(60,-20);
    osDelay(300);
    for(;stop_in_line();){
        osDelay(1);
    }
}
/*
 * 先循迹前进，在六边形顶点处左转任务
 */
void Moto::inway_left() const {
    inway(stop_in_center);
    frout((uint16_t)(700/v));
    Moto::speed(-20,60);
    osDelay(300);
    for(;stop_in_line();){
        osDelay(1);
    }
}
/*
 * 穿越白点任务
 */
void Moto::inway_crossing() const {
    inway(stop_in_center);
    frout((uint16_t)(400/v));
}
/*
 * 穿越中心拐角任务
 */
void Moto::inway_crossing_center() const {
    inway(stop_in_center);
    for(uint16_t t=0;t<500;t++){
        if(R1==0&&R2==1&R3==1)speed(40,100);
        else if(R1==1&&R2==1&R3==0)speed(100,40);
        else  speed(100,100);
        osDelay(1);
    }
}

/*
 * 放置物块到靶心任务
 */
void Moto::inway_stop_in_round_center(){
    inway_frout(800);
    inway(stop_in_round_center);
    vbuf=v;v=0.4;
    inway_frout(2600);
    v=vbuf;
    bcak((uint16_t)(700/v));
}
/*
 * 放置左方黑色物块到靶心任务
 */
void Moto::left_black_from_home() const {
    frout(800);
    left(stop_outof_line);
    frout(800);
    left(stop_in_line);
    frout(800);
    right(stop_outof_line);
}
/*
 * 放置右方白色物块到靶心任务
 */
void Moto::right_white_from_home() const {
    frout(800);
    right(stop_outof_line);
    frout(800);
    right(stop_in_line);
    frout(800);
    left(stop_outof_line);
}
/*
 * 放置左方白色物块到靶心任务
 */
void Moto::left_white_from_home() const {
    frout(500);
    left(stop_in_line);
    left(stop_in_line);
    left(stop_in_line);
    frout(500);
    left(stop_in_line);
    inway_crossing();
}
/*
 * 放置右方黑色物块到靶心任务
 */
void Moto::right_black_from_home() const {
    frout(500);
    right(stop_in_line);
    right(stop_in_line);
    right(stop_in_line);
    frout(500);
    right(stop_in_line);
    inway_crossing();
}
/*
 * 无用任务
 */
void Moto::debug() const{
    speed(100,100);
    osDelay(500);
}