//
// Created by 12097 on 2023/10/29.
//

#include "App.h"
#include "Moto.h"

#define  TIM_HANDLE  &htim3
#define mesure_enable  HAL_GPIO_WritePin(Trig_GPIO_Port,Trig_Pin,GPIO_PIN_SET)
#define mesure_disable  HAL_GPIO_WritePin(Trig_GPIO_Port,Trig_Pin,GPIO_PIN_RESET)

extern osThreadId defaultTaskHandle;
extern osThreadId StartHandle;
extern osThreadId LEDHandle;
extern osThreadId SR04Handle;

uint8_t key1;
char str[20];
float distance;      //测量距离

void delay_us(uint16_t nus){
    __HAL_TIM_SetCounter(TIM_HANDLE, 0); //把计数器的值设置为0
    __HAL_TIM_ENABLE(TIM_HANDLE); //开启计数
    while (__HAL_TIM_GET_COUNTER(TIM_HANDLE) < nus); //每计数一次，就是1us，直到计数器值等于我们需要的时间
    __HAL_TIM_DISABLE(TIM_HANDLE); //关闭计数
}

void measure_distance(){
    mesure_enable;
    //osDelay(1);
    delay_us(20);
    mesure_disable;
   __HAL_TIM_SetCounter(&htim1, 0); //把计数器的值设置为0
    __HAL_TIM_ENABLE(&htim1); //开启计数

}

//任务1
//按键扫描

void StartDefaultTask(void const * argument)
{
    /* USER CODE BEGIN StartDefaultTask */
    uint8_t k1;
    /* Infinite loop */
    for(;;)
    {
        osDelay(20);
        if (HAL_GPIO_ReadPin(key_GPIO_Port,key_Pin)==1&&k1>0) {
            k1++;
            if (k1>5) {
                key1=1;
                k1=0;
                osDelay(50);
            }
        } else if (HAL_GPIO_ReadPin(key_GPIO_Port,key_Pin)==0)k1=1;
    }
    /* USER CODE END StartDefaultTask */
}

//任务2
//主任务

void StartTask(void const * argument)
{
    Moto::init();
    Moto contrl{};
    Moto::v=1.2;
    contrl.step=1;
    contrl.rref=1.1;
    contrl.lref=1;
    while (key1!=1)osDelay(10);
    contrl.speed(100,100);
    contrl.Moto_app();
    contrl.speed(0,0);//*/
}

void LEDTask(void const * argument){
    for (;;){
        HAL_GPIO_TogglePin(LED_GPIO_Port,LED_Pin);
        osDelay(800);
    }
}

void SRTask(void const * argument) {
    for (;;) {
        measure_distance();
        osDelay(80);
    }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){

    uint16_t cnt = 0;
    float time=0.0f;
    if(GPIO_Pin==GPIO_PIN_5)
    {
        __HAL_TIM_DISABLE(&htim1); //关闭计数
        cnt= TIM1->CNT;
        time=cnt*0.01f;
        cnt=0;
        distance=time*0.340f/2.0f*10.0f;
        distance=distance<0?0:distance;
        //HAL_UART_Transmit(&huart1,(uint8_t *)str,sprintf(str,"%f\r\n",distance),10);
    }
}
